package Pics;

import com.sun.lwuit.Component;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.list.ListCellRenderer;
import com.sun.lwuit.plaf.Border;
import java.io.IOException;

public class MyListRenderer extends Label implements ListCellRenderer {

    private Image[] images;
    /** Creates a new instance of MyListRenderer */

    public MyListRenderer() {
        super("");
        images = new Image[4];
        try {
            images[0] = Image.createImage("/pics.jpg");
            images[1] = Image.createImage("/about.png");
//            images[2] = Image.createImage("/helpinfo.png");
//            images[3] = Image.createImage("/services.png");
//
        } catch (IOException ex) {
        }
    }

    public Component getListCellRendererComponent(List list, Object value, int index, boolean isSelected) {
        setText(value.toString());
        //getStyle().setFont(Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD,Font.SIZE_MEDIUM));
        if (index == 0) {
            setFocus(true);
            setIcon(images[0]);
            getStyle().setBgColor(0xffcc99);
            getStyle().setBgTransparency(55);
            getStyle().setBorder(Border.createRoundBorder(15, 15, 0xff9900, true));
               
            
        } 
        else if (index == 1) {
            setFocus(true);
            setIcon(images[1]);
            getStyle().setBgColor(0xffcc99);
            getStyle().setBgTransparency(55);
            getStyle().setBorder(Border.createRoundBorder(15, 15, 0xff9900, true));
     
        }
//            else if (index == 2) {
//            setFocus(true);
//            //setIcon(images[2]);
//            getStyle().setBgColor(0xffcc99);
//            getStyle().setBgTransparency(55);
//            getStyle().setBorder(Border.createRoundBorder(15, 15, 0xff9900, true));
//     
//        }
//        
//            else if (index == 3) {
//            setFocus(true);
//            //setIcon(images[3]);
//            getStyle().setBgColor(0xffcc99);
//            getStyle().setBgTransparency(55);
//            getStyle().setBorder(Border.createRoundBorder(15, 15, 0xff9900, true));
//            }
        return this;
    }
      
    public Component getListFocusComponent(List list) {
        //setIcon(images[2]);
        setText("");
        getStyle().setBgColor(0x0000ff);//no effect
        setFocus(true);
        getStyle().setBgTransparency(100);
        return this;
    }
}
