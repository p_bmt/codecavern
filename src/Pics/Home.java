/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Pics;

import com.downjuu.ads.AdBanner;
import com.sun.lwuit.Button;
import com.sun.lwuit.Command;
import com.sun.lwuit.Container;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.List;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.layouts.BoxLayout;
import com.sun.lwuit.plaf.Border;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;



public class Home {
    
    private Form frmHome;
    public List lsHome;
    private AdBanner ad1, ad2, ad3;
    private Label lblhome;
    
    //private Button btninfo, btnexit;
    
    String [] listitems = {"Photos", "About App"};
    
    
    XmasPics object;

    public Home(XmasPics midletrefference) {
        object = midletrefference;
    }
    
    public void Home(){
        
        frmHome = new Form("Christmas Photos");
        frmHome.setLayout(new BorderLayout());
        
        ad1 = new AdBanner(object);
        ad1.getStyle().setBgTransparency(0);
        ad1.requestAd();
        try {
            lblhome = new Label(Image.createImage("/home.jpg"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        lsHome = new List(listitems);
        lsHome.setListCellRenderer(new MyListRenderer());
        lsHome.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                int index = lsHome.getSelectedIndex();
                if(index== 0){
                    Global.pagesum = 0;
                    PicsList(Global.pagesum);
                }

                else if(index == 1){
                    About();
                }
            }
        });
            
        Container contHome = new Container();
        contHome.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        contHome.addComponent(lblhome);
        contHome.addComponent(lsHome);
        
        frmHome.addCommand(new Command("Ëxit"){
            public void actionPerformed(ActionEvent e){
                    object.destroyApp(true);
                    object.notifyDestroyed();
                }
        });
        
        frmHome.addComponent(BorderLayout.NORTH,ad1);
        frmHome.addComponent(BorderLayout.CENTER, contHome);
        frmHome.show();
    }
    
    
    
    
    
    
    
    private Form frmGroupPics;
    private Button Pic1, Pic2, Pic3, Pic4, Pic5;
    private Button btnnext1;
    com.sun.lwuit.Image testimage1,testimage2,testimage3,testimage4,testimage5,testimage6;
    
    public void PicsList(final int counter){
        
        showProgress();
        
        frmGroupPics = new Form("Photos");
        frmGroupPics.setLayout(new BorderLayout());
        
        ad2 = new AdBanner(object);
        ad2.getStyle().setBgTransparency(0);
        ad2.requestAd();
        
        ad3 = new AdBanner(object);
        ad3.getStyle().setBgTransparency(0);
        ad3.requestAd();
        
        
        Global.name = Integer.toString(counter+1);
        Global.name = Global.name+".jpg";
                
        try {
            testimage1 = getImage(Global.name,1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Pic1 = new Button(null,testimage1);
        
        Pic1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                              
                Global.name = Integer.toString(counter+1);
                Global.name = Global.name+".jpg";
                ImageShow(Global.name,2);
            }
        });
        
        Global.name = Integer.toString(counter+2);
        Global.name = Global.name+".jpg";
                
        try {
            testimage2 = getImage(Global.name,1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Pic2 = new Button(null,testimage2);
        
        Pic2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                Global.name = Integer.toString(counter+2);
                Global.name = Global.name+".jpg";
                ImageShow(Global.name,2);
            }
        });

        
        Global.name = Integer.toString(counter+3);
        Global.name = Global.name+".jpg";
                
        try {
            testimage3 = getImage(Global.name,1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Pic3 = new Button(null,testimage3);
        
        Pic3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                Global.name = Integer.toString(counter+3);
                Global.name = Global.name+".jpg";
                ImageShow(Global.name,2);
            }
        });
        
        Global.name = Integer.toString(counter+4);
        Global.name = Global.name+".jpg";
                
        try {
            testimage4 = getImage(Global.name,1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Pic4 = new Button(null,testimage4);
        
        Pic4.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                Global.name = Integer.toString(counter+4);
                Global.name = Global.name+".jpg";
                ImageShow(Global.name,2);
            }
        });
        
        
        Global.name = Integer.toString(counter+5);
        Global.name = Global.name+".jpg";
                
        try {
            testimage5 = getImage(Global.name,1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Pic5 = new Button(null,testimage5);
        
        Pic5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                Global.name = Integer.toString(counter+5);
                Global.name = Global.name+".jpg";
                ImageShow(Global.name,2);
            }
        });
        
//        Container contTop = new Container();
//        contTop.setLayout(new BoxLayout(BoxLayout.X_AXIS));
        
        Container contMid = new Container();
        contMid.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        contMid.addComponent(Pic1);
        contMid.addComponent(Pic2);
        contMid.addComponent(Pic3);
        contMid.addComponent(Pic4);
        contMid.addComponent(Pic5);
        contMid.addComponent(ad3);
        
        btnnext1 = new Button("Next");
        btnnext1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                
                if(Global.pagesum >=20){
                Dialog a = new Dialog("No other page");
                a.setTimeout(2000);
                a.show(0, 50, 11, 11, true);
                }
                else{
                Global.pagesum = Global.pagesum+5;
                PicsList(Global.pagesum);
                }
            }
        });
        
        btnback2 = new Button("Previous");
        btnback2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                
                if(Global.pagesum <= 0){
                    //btnback2.isEnabled() = false;
                Dialog a = new Dialog("No previous page");
                a.setTimeout(2000);
                a.show(0, 100, 11, 11, true);
                }
                else{
                Global.pagesum = Global.pagesum-5;
                PicsList(Global.pagesum);
                }                
            }
        });
        
        
        Container contBottom = new Container();
        contBottom.setLayout(new BoxLayout(BoxLayout.X_AXIS));
        contBottom.addComponent(btnback2);
        contBottom.addComponent(btnnext1);
        
        
        frmGroupPics.addCommand(new Command("Back"){
                public void actionPerformed(ActionEvent e){
                    Home();
                }
        });
        
        frmGroupPics.setScrollableY(true);
        frmGroupPics.setScrollVisible(true);
        frmGroupPics.addComponent(BorderLayout.NORTH,ad2);
        frmGroupPics.addComponent(BorderLayout.CENTER,contMid);
        frmGroupPics.addComponent(BorderLayout.SOUTH,contBottom);
        frmGroupPics.show();
        
    }
    
    
    
    
    
    
    
    
    
    private Form frmImageShow;
    private Label lblpic;
    private Button btnback2;
    AdBanner ad4;
    com.sun.lwuit.Image imageMain;
    
    public void ImageShow(String img, int opt){
        
        showProgress();
        
        frmImageShow = new Form("Show Image");
        frmImageShow.setLayout(new BorderLayout());
        frmImageShow.setScrollable(false);
        
        ad4 = new AdBanner(object);
        ad4.getStyle().setBgTransparency(0);
        ad4.requestAd();

        try {
            imageMain = getImage(img,opt);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        lblpic = new Label(imageMain);
        lblpic.isScrollableX();
        lblpic.isScrollableY();

        Container contImage = new Container();
        contImage.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        contImage.addComponent(lblpic);
        
        frmImageShow.addCommand(new Command("Back"){
                public void actionPerformed(ActionEvent e){
                    PicsList(Global.pagesum);
                }
        });
        
        frmImageShow.addCommand(new Command("Download"){
            public void actionPerformed(ActionEvent arg0){
                    try{
                        //String ext = Integer.toString(Global.counter)+".jpg";
                        String URL = "http://devs.mobi/paul/christmas/large/"+Global.name;
                        
                        if (object.platformRequest(URL))
                         System.out.println();
              }
             catch(ConnectionNotFoundException e){
             }
                }
        });
        

        frmImageShow.addComponent(BorderLayout.CENTER, contImage);
        frmImageShow.addComponent(BorderLayout.SOUTH, ad4);
        frmImageShow.show();

}
    
    
    
    
    
    
    private Form frmAbout;
    private TextArea taAbout;
    private AdBanner adabout;

    public void About(){

        showProgress();

        adabout = new AdBanner(object);
        adabout.requestAd();


        frmAbout = new Form("About App");
        frmAbout.setLayout(new BorderLayout());
        frmAbout.setScrollable(false);

        frmAbout.addComponent(BorderLayout.NORTH, adabout);

        String about = ReadTextFiles("/about.txt");

        taAbout = new TextArea(about);
        taAbout.setEditable(false);
        taAbout.isScrollableX();

        Container contAbout = new Container();
        contAbout.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        contAbout.addComponent(taAbout);

        frmAbout.addCommand(new Command("Back"){
                public void actionPerformed(ActionEvent e){
                    Home();
                }
        });

        frmAbout.addComponent(BorderLayout.CENTER, contAbout);
        frmAbout.show();
    }
    
    
    
    
    
    
    private Image getImage(String img, int opt) throws IOException{
        String urlpath = null;
        if(opt == 1){
            urlpath="http://devs.mobi/paul/christmas/small/";
        }
        else if(opt==2){
            urlpath ="http://devs.mobi/paul/christmas/large/";
        }
     String url =urlpath+img;

      HttpConnection http = (HttpConnection) Connector.open(url);


       DataInputStream iStrm = http.openDataInputStream();
       ByteArrayOutputStream bStrm = null;
       Image im = null;

       try
       {
         // ContentConnection includes a length method
         byte imageData[];
         int length = (int) http.getLength();
         if (length != -1)
         {
           imageData = new byte[length];

           // Read the png into an array
   //        iStrm.read(imageData);
           iStrm.readFully(imageData);
         }
         else  // Length not available...
         {
           bStrm = new ByteArrayOutputStream();

           int ch;
           while ((ch = iStrm.read()) != -1)
             bStrm.write(ch);

           imageData = bStrm.toByteArray();
           bStrm.close();
         }

         // Create the image from the byte array
         im = Image.createImage(imageData, 0, imageData.length);
       }
       finally
       {
         // Clean up
         if (iStrm != null)
           iStrm.close();
         if (http != null)
           http.close();
         if (bStrm != null)
           bStrm.close();
       }
       return (im == null ? null : im);
   }
    
    
    Dialog progress;
    Label dialogLabel;
    
    public void showProgress() {
        progress = new Dialog();
        progress.getDialogStyle().setBorder(Border.createRoundBorder(6, 6, 0xe3ef5a));
        progress.getDialogStyle().setBgColor(0xFE2E2E);
        progress.getDialogStyle().setFgColor(0xffffff);
        progress.setTransitionInAnimator(CommonTransitions.createSlide(CommonTransitions.SLIDE_VERTICAL, true, 700));
        progress.setTransitionOutAnimator(CommonTransitions.createSlide(CommonTransitions.SLIDE_VERTICAL, false, 700));
        dialogLabel = new Label("      Loading, please wait...");
        dialogLabel.setAlignment(Label.CENTER);
        dialogLabel.getStyle().setBgTransparency(0);
        progress.addComponent(dialogLabel);
        progress.addCommand(new Command("Cancel") {

            public void actionPerformed(ActionEvent ev) {
                progress.dispose();

            }
        });
        int height = Display.getInstance().getDisplayHeight() - (progress.getContentPane().getPreferredH() + progress.getTitleComponent().getPreferredH());
        height /= 2.1;
        progress.show(height, height - 18, 20, 20, true, false);
    }
    
    //this method accepts a string variable - the name of the file to be opened
 public String ReadTextFiles(String textfilename)
 {
     //input stream allows you to read data from a file or other stream - 'textfilename' the name receive the methodd by
       InputStream is=getClass().getResourceAsStream("/"+textfilename);

        StringBuffer sb=new StringBuffer();
        int chars;
        try {//using inpt stream - read all the characters for the text file
            while ((chars = is.read()) != -1) {
                sb.append((char) chars);//adding all the read characters to string buffer
            }
               return sb.toString();//return a buffer of all characters

        } catch (IOException ex) {
         return null;
        }

 }
}
