package Pics;



import Pics.XmasPics;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * @author Leonard Swigo
 *
 */
//class inherits from Canvas
public class Splash extends Canvas {
//declare variables
private int height;
private int  width;
private int current= 0;
private int  factor;
private Timer timer = new Timer();
//initialize image
Image sp;
   XmasPics midletrefference;//create our midlet object
//receives the midlet 
    public Splash(XmasPics myapp){
//link the created midlet object and the midlet context we received
     this.midletrefference =myapp;
//stylr the canvas
        setFullScreenMode(true);
        height= getHeight();
        width = this.getWidth();
        factor = width/150;
        repaint();
        //call draw method - in this class
        timer.schedule(new draw(), 160,60);

    }
protected void paint(Graphics g) {
        try{
//image to be displayed in the splash screen
        sp = javax.microedition.lcdui.Image.createImage("/main.png");
     
        }catch(IOException io)
        {
        }

    //for the background
  g.setColor(226, 226, 255);
  g.fillRect(UP, UP, width, height);
  g.drawImage(sp,  getWidth()/2, getHeight()/2,
  javax.microedition.lcdui.Graphics.VCENTER|javax.microedition.lcdui.Graphics.HCENTER);
 
  //The loading Arc/text color
  g.setColor(0x70,223,234);
     
  g.fillRect((width/4), (height/2)+72, current, 10);
  g.drawString("Loading....", (width/2)-4, (height/2)+100, Graphics.BOTTOM|Graphics.HCENTER);
}
//draw the splash screen
private class draw extends TimerTask{
public void run()
{
            current  = current + factor;
            if(current> width - width/1.8)
            {

                timer.cancel();
                //when the timer cancels - call main menu in the midlet
                
              Home e = new Home(midletrefference);
              e.Home();

            }
            else
            {
            repaint();
            }
         }

    }

}